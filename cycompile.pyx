#!/usr/bin/env python3
from os import system, getcwd as cwd, path, walk, listdir
from subprocess import getoutput
from sys import argv

if '-f' not in argv and ('-cythonise' in argv or '-cythonize' in argv): pass
elif '-f' in argv and ('-cythonise' not in argv or '-cythonize' not in argv):
    if not path.exists(argv[argv.index('-f')+1]): raise SystemExit("Path does not exist")

    if argv[argv.index('-f')+1].endswith(".py"):
        ext = "py"
    elif argv[argv.index('-f')+1].endswith(".pyx"):
        ext = "pyx"
    else:
        raise SystemExit("File does not end with .py or .pyx")
    if ext == "pyx":
        file_name = argv[argv.index('-f')+1].split('/')[-1][:-4]
    if ext == "py":
        file_name = argv[argv.index('-f')+1].split('/')[-1][:-3]
    path_to_file = ''
    for i in argv[argv.index('-f')+1].split('/').pop(argv[argv.index('-f')+1].split('/').index(file_name+"."+ext)):
        path_to_file = path_to_file.join(i)

elif '-f' not in argv and ('-cythonise' not in argv or '-cythonize' not in argv):
    raise SystemExit('Path to file/folder not specified, please do \'cycompile -f {file_to_compile}\'')
elif '-f' in argv and ('-cythonise' in argv or '-cythonize' in argv): raise SystemExit('As of now, the script doesn\'t support compiling from a specified location') 

def syscall(cmd: str) -> None: system('cd '+path_to_file+' && '+cmd)

def add_args(l: list) -> None: argv.extend(l)

try:
    if argv[4]:
        for arg in argv:
            if '-kc' in argv: argv.pop(argv.index('-kc')); add_args(['-c', '-k'])
            elif '-ck' in argv: argv.pop(argv.index('-ck')); add_args(['-c', '-k'])
            if '-cythonise' in argv: argv.pop(argv.index('-cythonise')); add_args(['-cythonize'])

            if arg not in [argv[0], argv[1], '-k', '-c', '-clang', '-so', '-f', '-cythonize']:
                raise SystemExit('Invalid Argument')
except IndexError: pass

if '-c' in argv: file_type = 'C'
else: file_type = 'C++'

if file_type == 'C++' and '-clang' in argv: typeext = 'cpp'; compiler = 'clang++'
elif file_type == 'C' and '-clang' in argv: typeext = 'c'; compiler = 'clang'
elif file_type == 'C++' and '-clang' not in argv: typeext = 'cpp'; compiler = 'g++'
elif file_type == 'C' and '-clang' not in argv: typeext = 'c'; compiler = 'gcc'

if '-f' in argv:
    print("Making the {} file...".format(file_type))
    if '-so' not in argv:
        embed = ' --embed'
    else:
        embed = ''
    if file_type == 'C++': syscall('cython -3'+embed+' --cplus '+argv[argv.index('-f')+1])
    elif file_type == "C": syscall('cython -3'+embed+'  '+argv[argv.index('-f')+1])
    print("Finished making {}.{}".format(file_name, typeext))

    if '-so' in argv:
        print("Creating a .so file...")
        syscall("{} -fPIC -c -Wall $(pkg-config --cflags --libs python3) {}.{}".format(compiler, file_name, typeext))
        syscall("ld -shared {0}.o -o {0}.so".format(file_name))
        print("Finished building {}.so".format(file_name))
        end_msg = "You can now import this file by doing 'import {}'".format(file_name)
    elif '-so' not in argv:
        print("Compiling the {} code...".format(file_type))
        syscall('{0} {1}.{2} $(pkg-config --cflags --libs python3) -o {1}'.format(compiler, file_name, typeext))
        print("Finished compiling {}".format(file_name))
        end_msg = "You can now run this file by doing './{}'".format(file_name)
    if '-k' not in argv:
        print("Cleaning up...")
        getoutput('cd {0} && rm {1}.cpp {1}.c {1}.o'.format(cwd(), file_name))
        print("Finished cleaning up")
    print(end_msg)
    del(syscall, system, file_name, file_type, cwd, typeext, compiler, getoutput)

if '-cythonize' in argv:
    print('Compiling the module...')
    syscall('cd .. && cp -r {0} {0}_compiled'.format(cwd()))
    def cythonise(root, files):
        for file in files:
            flags = ' '
            if '-k' in argv: flags = flags + '-k '
            if '-c' in argv: flags = flags + '-c '
            if '-clang' in argv: flags = flags + '-clang '
            if file == '__init__.py':
                pass
            elif file.endswith('.py') or file.endswith('.pyx'):
                if flags == ' ': output = getoutput('cd {}_compiled && cd {} && {} -so -f {}'.format(cwd(), root, argv[0], file))
                elif flags != ' ': output = getoutput('cd {}_compiled && cd {} && {} -so -f {}{}'.format(cwd(), root, argv[0], file, flags))    
                if 'error' in output:
                    raise SystemExit('Error in the code, stopping the compiler, do not use the current packages as they are, first fix the errors, then run the code. Here is the error:\n{}'.format(output))
                del(output)

    for root, dirs, files in walk('.'):
        if files != []:
            cythonise(root, files)
    end_msg = 'Module has been compiled, try importing the module, if this does not work, please contact Proxy'
    print(end_msg)
    del(syscall, system, cwd, typeext, compiler, getoutput)

del(argv)
